use std::{
    collections::HashMap,
    ffi::{OsStr, OsString},
    io::{Error, ErrorKind},
    path::{Component, Path},
};

use super::implementation::{Inode, InodeInner, VirtualDiskitInner};

impl VirtualDiskitInner
{
    pub fn get_inode_by_id(&self, id: usize) -> Result<&Inode, Error>
    {
        if self.content.len() <= id
        {
            Err(ErrorKind::NotFound.into())
        }
        else
        {
            Ok(&self.content[id])
        }
    }

    pub fn get_mut_inode_by_id(&mut self, id: usize) -> Result<&mut Inode, Error>
    {
        if self.content.len() <= id
        {
            Err(ErrorKind::NotFound.into())
        }
        else
        {
            Ok(&mut self.content[id])
        }
    }

    pub fn get_inode_by_path_and_dir(
        &self,
        dir: &HashMap<OsString, usize>,
        path: &OsStr,
    ) -> Result<&Inode, Error>
    {
        self.get_inode_by_id(*dir.get(path).ok_or(ErrorKind::NotFound)?)
    }

    pub fn get_inode_by_path_and_inode(&self, inode: &Inode, path: &OsStr)
        -> Result<&Inode, Error>
    {
        if let InodeInner::Dir(dir) = &inode.inner
        {
            self.get_inode_by_path_and_dir(dir, path)
        }
        else
        {
            Err(From::from(ErrorKind::NotADirectory))
        }
    }

    pub fn get_first_walkdir_pos(
        &self,
        path: &Path,
        contents_first: bool,
    ) -> Result<Vec<usize>, Error>
    {
        if !contents_first
        {
            return Ok(vec![]);
        }

        let mut inode = self
            .get_inode_by_full_path(path)
            .and_then(|x| x.map_err(|_| ErrorKind::NotFound.into()))?;
        let mut rv = 0;

        while let Some((_, inode_id)) = Self::get_dir_as_iterator(inode)
            .ok()
            .and_then(|mut x| x.next())
        {
            inode = self.get_inode_by_id(*inode_id)?;
            rv += 1;
        }

        Ok(vec![0; rv])
    }

    pub fn get_dir_from_inode(inode: &Inode) -> Result<&HashMap<OsString, usize>, Error>
    {
        match &inode.inner
        {
            InodeInner::Dir(dir) => Ok(dir),
            _ => Err(ErrorKind::NotADirectory.into()),
        }
    }

    pub fn get_reference_to_pwd(&self) -> Result<&Inode, Error>
    {
        let mut inode = &self.content[0];

        for component in self.pwd.components().skip(1)
        {
            match component
            {
                Component::ParentDir =>
                {
                    inode = self.get_inode_by_path_and_dir(
                        Self::get_dir_from_inode(inode)?,
                        OsStr::new(".."),
                    )?;
                }
                Component::Normal(path) =>
                {
                    inode =
                        self.get_inode_by_path_and_dir(Self::get_dir_from_inode(inode)?, path)?;
                }
                // Does nothing
                Component::CurDir =>
                {}
                // As far a I understood it, Component::RootDir can
                // only happen if there is a prefix (which is only
                // possible under windows), because the first element
                // is skipped in the for loop.
                _ => panic!("Windows is not supported"),
            }
        }

        // Guarantees that the pwd is actually a directory.
        let _ = Self::get_dir_from_inode(inode)?;

        Ok(inode)
    }

    pub fn get_inode_by_full_path(&self, path: &Path) -> Result<Result<&Inode, &Inode>, Error>
    {
        let mut inode = if path.starts_with("/")
        {
            &self.content[0]
        }
        else
        {
            self.get_reference_to_pwd()?
        };

        let mut not_found = false;

        for component in path.components()
        {
            if not_found
            {
                return Err(ErrorKind::NotFound.into());
            }

            if !matches!(inode.inner, InodeInner::Dir(_))
            {
                // TODO: Check if this line is correct.
                return Err(ErrorKind::NotADirectory.into());
            }

            inode = match component
            {
                Component::CurDir => self.get_inode_by_path_and_inode(inode, OsStr::new("."))?,
                Component::ParentDir =>
                {
                    self.get_inode_by_path_and_inode(inode, OsStr::new(".."))?
                }
                Component::Normal(path) =>
                {
                    let val = self.get_inode_by_path_and_inode(inode, path);

                    if val
                        .as_ref()
                        .err()
                        .map_or(false, |x| x.kind() == ErrorKind::NotFound)
                    {
                        not_found = true;
                        continue;
                    }

                    val?
                }
                Component::RootDir => &self.content[0],
                Component::Prefix(_) => panic!("Windows is not supported"),
            };
        }

        if not_found
        {
            return Ok(Err(inode));
        }

        Ok(Ok(inode))
    }

    pub fn get_dir_as_iterator(
        inode: &Inode,
    ) -> Result<impl Iterator<Item = (&OsString, &usize)>, Error>
    {
        Ok(Self::get_dir_from_inode(inode)?
            .iter()
            .filter(|(path, _)| *path != "." && *path != ".."))
    }

    pub fn check_pos_path(&self, root_inode: &Inode, pos: &[usize]) -> Option<()>
    {
        let mut inode = root_inode;

        for &index in pos
        {
            let (_, &content_inode) = Self::get_dir_as_iterator(inode).ok()?.nth(index)?;

            inode = self.get_inode_by_id(content_inode).ok()?;
        }

        Some(())
    }
}

// These methods are only needed if the `trash` feature is activated.
#[cfg(feature = "trash")]
impl VirtualDiskitInner
{
    pub fn get_mut_dir_from_inode(inode: &mut Inode)
        -> Result<&mut HashMap<OsString, usize>, Error>
    {
        match &mut inode.inner
        {
            InodeInner::Dir(dir) => Ok(dir),
            _ => Err(ErrorKind::NotADirectory.into()),
        }
    }

    pub fn get_dir_by_id(&self, inode: usize) -> Result<&HashMap<OsString, usize>, Error>
    {
        match &self.content[inode].inner
        {
            InodeInner::Dir(dir) => Ok(dir),
            _ => Err(ErrorKind::NotADirectory.into()),
        }
    }

    pub fn get_mut_inode_by_full_path(&mut self, path: &Path) -> Result<&mut Inode, Error>
    {
        self.get_mut_inode_by_id(
            self.get_inode_by_full_path(path)?
                .map_err(|_| ErrorKind::NotFound)?
                .id,
        )
    }

    pub fn collect_inodes(&self, inode: &Inode, vec: &mut Vec<usize>) -> Result<(), Error>
    {
        vec.push(inode.id);
        if let InodeInner::Dir(dir) = &inode.inner
        {
            dir.iter()
                .map(|(_, &id)| {
                    self.get_inode_by_id(id)
                        .and_then(|inode| self.collect_inodes(inode, vec))
                })
                .collect::<Result<Vec<_>, _>>()?;
        }

        Ok(())
    }
}
