//! Always failling diskit
//!
//! For more information see the [struct level documentation](VoidDiskit).

use std::{
    io::{Error, ErrorKind, SeekFrom},
    path::{Path, PathBuf},
};

use crate::{
    dir_entry::DirEntry,
    file::{File, FileInner},
    metadata::Metadata,
    open_options::OpenOptions,
    walkdir::{WalkDir, WalkdirIterator, WalkdirIteratorInner},
    Diskit,
};

/// Always failling diskit
///
/// Every operation with this diskit fails with an
/// [`Error::Unsupported`](`std::io::ErrorKind::Unsupported`).
#[derive(Debug, Clone, Copy)]
pub struct VoidDiskit;

impl Default for VoidDiskit
{
    fn default() -> Self
    {
        Self
    }
}

impl Diskit for VoidDiskit
{
    fn set_pwd_inner(&self, _: &Path) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn get_pwd(&self) -> Result<PathBuf, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn open_inner(&self, _: &Path) -> Result<File<Self>, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn create_inner(&self, _: &Path) -> Result<File<Self>, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn open_with_options_inner(&self, _: &Path, _: OpenOptions) -> Result<File<Self>, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn read_inner(&self, _: &FileInner, _: &mut [u8]) -> Result<usize, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn read_to_end_inner(&self, _: &mut FileInner, _: &mut Vec<u8>) -> Result<usize, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn read_to_string_inner(&self, _: &mut FileInner, _: &mut String) -> Result<usize, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn write_inner(&self, _: &mut FileInner, _: &[u8]) -> Result<usize, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn write_all_inner(&self, _: &mut FileInner, _: &[u8]) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn flush_inner(&self, _: &mut FileInner) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn metadata_inner(&self, _: &FileInner) -> Result<Metadata, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn seek_inner(&self, _: &mut FileInner, _: SeekFrom) -> Result<u64, Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn create_dir_inner(&self, _: &Path) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn create_dir_all_inner(&self, _: &Path) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn close_inner(&self, _: FileInner) -> Result<(), Error>
    {
        Err(ErrorKind::Unsupported.into())
    }

    fn walkdir_inner(&self, _: &Path) -> WalkDir<Self>
    {
        WalkDir::new_default(*self, PathBuf::new())
    }

    fn into_walkdir_iterator(&self, _: WalkDir<Self>) -> WalkdirIterator<Self>
    {
        WalkdirIterator {
            inner: Err(ErrorKind::Unsupported.into()),
        }
    }

    fn walkdir_next_inner(&self, _: &mut WalkdirIteratorInner) -> Option<Result<DirEntry, Error>>
    {
        Some(Err(ErrorKind::Unsupported.into()))
    }

    #[cfg(feature = "trash")]
    fn trash_delete_inner(&self, _: &Path) -> Result<(), trash::Error>
    {
        Err(trash::into_unknown("Operation not supported"))
    }
}
