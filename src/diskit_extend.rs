//! Trait to make request interception usable
//!
//! For more information see the [trait level documentation](DiskitExt).

use std::{
    io::{Error, Read},
    path::Path,
};

use crate::{file::File, open_options::OpenOptions, walkdir::WalkDir, Diskit};

mod sealed
{
    pub trait Sealed {}
}

impl<T> sealed::Sealed for T where T: Diskit {}

/// Trait to make request interception usable
///
/// This trait gives a nicer looking interface to most functions of
/// [`Diskit`](crate::Diskit).  Please refer to it's documentation.
pub trait DiskitExt: Diskit + sealed::Sealed
{
    /// Changes the current working directory
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::set_pwd_inner)'s documentation.
    fn set_pwd<P: AsRef<Path>>(&self, path: P) -> Result<(), Error>
    {
        self.set_pwd_inner(path.as_ref())
    }

    /// Changes the current working directory
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::open_inner)'s documentation.
    fn open<P: AsRef<Path>>(&self, path: P) -> Result<File<Self>, Error>
    {
        self.open_inner(path.as_ref())
    }

    /// Creates a new file or truncates it
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::create_inner)'s documentation.
    fn create<P: AsRef<Path>>(&self, path: P) -> Result<File<Self>, Error>
    {
        self.create_inner(path.as_ref())
    }

    /// Opens a file with custom options
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::open_with_options_inner)'s documentation.
    fn open_with_options<P: AsRef<Path>>(
        &self,
        path: P,
        options: OpenOptions,
    ) -> Result<File<Self>, Error>
    {
        self.open_with_options_inner(path.as_ref(), options)
    }

    /// Creates a directory
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::create_dir_inner)'s documentation.
    fn create_dir<P: AsRef<Path>>(&self, path: P) -> Result<(), Error>
    {
        self.create_dir_inner(path.as_ref())
    }

    /// Creates a directory and all above if necessary
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::create_dir_all_inner)'s documentation.
    fn create_dir_all<P: AsRef<Path>>(&self, path: P) -> Result<(), Error>
    {
        self.create_dir_all_inner(path.as_ref())
    }

    /// Creates a [`Walkdir`](crate::walkdir::WalkDir) from a path
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::walkdir_inner)'s documentation.
    fn walkdir<P: AsRef<Path>>(&self, path: P) -> WalkDir<Self>
    {
        self.walkdir_inner(path.as_ref())
    }

    /// Moves a file or directory in the the trash
    ///
    /// For more information please see
    /// [`Diskit`](crate::Diskit::trash_delete_inner)'s documentation.
    #[cfg(feature = "trash")]
    fn trash_delete<P: AsRef<Path>>(&self, path: P) -> Result<(), trash::Error>
    {
        self.trash_delete_inner(path.as_ref())
    }

    /// Reads file to end
    ///
    /// This function opens the file, reads it in a buffer and returns
    /// it.  This is a convenience method build upon
    /// [`File::read_to_end`](crate::file::File::read_to_end) similar
    /// to [`std::fs::read_to_string`].
    fn read_to_end<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>, Error>
    {
        let mut buf = vec![];

        self.open(path)?.read_to_end(&mut buf)?;

        Ok(buf)
    }

    /// Reads file to end in string
    ///
    /// This function opens the file, reads it in a string buffer and
    /// returns it.  This is a convenience method build upon
    /// [`File::read_to_string`](crate::file::File::read_to_string)
    /// similar to [`std::fs::read_to_string`].
    fn read_to_string<P: AsRef<Path>>(&self, path: P) -> Result<String, Error>
    {
        let mut buf = String::new();

        self.open(path)?.read_to_string(&mut buf)?;

        Ok(buf)
    }
}

impl<T> DiskitExt for T where T: Diskit {}
