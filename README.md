# Utilities for intercepting disk requests

Diskit (short for "**Dis**c root **kit**") attempts to be an
intransparent, rust-style root kit for intercepting and modifying
requests to the hard drive.  To use it, decide what you want to do
with the intercepted requests and choose the appropriate diskit
(e.g.: no interception -> [`StdDiskit`]; redirect to virtual file
system -> [`VirtualDiskit`]; logging all requests ->
[`LogDiskit`]), then route all requests through it:
```rust
use std::io::{Read, Write};
use diskit::{diskit_extend::DiskitExt, Diskit, VirtualDiskit};

fn main() -> Result<(), std::io::Error>
{
    // All requests are redircted to a virtual file system.
    let diskit = VirtualDiskit::default();

    // This writes "Hello, World!" in the newly created file "test.txt".
    let mut file1 = diskit.create("test.txt")?;
    file1.write_all(b"Hello, World!")?;

    // You can close `file1` and it still works:
    // file1.close()?;

    // This reads the just created file.
    let mut file2 = diskit.open("test.txt")?;
    let mut buf = String::new();
    file2.read_to_string(&mut buf)?;

    assert_eq!(buf, "Hello, World!");

    Ok(())
}
```
If you want to see how diskit would be used in a full program, see
[legacylisten](https://codeberg.org/zvavybir/legacylisten), the
program I wrote diskit for.

# Making your own diskit
You can make your own diskit by implementing the [`Diskit`] trait.

Because a lot of [stdlib](std) types are intransparent, there are
transparent replicas of them here that you're going to have to
use.  Most of these types have an inner type
(e.g. [`File`](file::File) has [`FileInner`](file::FileInner)) to
make the [`Diskit`] trait object-safe and this library easy to
use.

To make [`StdDiskit`] as overheadless as possible most types have an
`Option<StdsVersionOfThisType>`, which should be [`None`] in your
implementation.

If your diskit internally still needs to access the disk, please
consider delegating these to an other diskit like [`LogDiskit`]
does.

# Size of a diskit
Diskits *should* (there is no way or reason to enforce it) be as
small and cheaply [clone](Clone)able as possible.  This is because
this library should be optimized for [`StdDiskit`] (as this is
expected to be it's most used – albeit most useless – diskit) and
while adding diskit support to
[legacylisten](https://crates.io/crates/legacylisten) it became
apparent that for good usability and performance diskits are
[clone](Clone)d often and passed by value.

If your diskit is bigger than one [`usize`] consider wrapping it
in an [`Arc`](std::sync::Arc).

# Stability
Diskit is still in a very early version and nothing is stable,
although I *try* to keep it stable.

# Contributing
I have written that library specifically for
[legacylisten](https://crates.io/crates/legacylisten) and added
(mostly) only what I needed for that, so it's still lacking in a
lot for normal usage.  If you need an additional feature (or
features) or otherwise have an idea how to make it better, please
don't hesitate to
[share](https://codeberg.org/zvavybir/diskit/issues/new) it or
[implement](https://codeberg.org/zvavybir/diskit/pulls) it
yourself.
